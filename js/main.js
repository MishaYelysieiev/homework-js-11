let listAmount = +prompt("Enter the number of items");

const listContent = [];

for(let i=0;i<listAmount;i++){
    let items = prompt("Enter the item #"+(i+1));
    listContent[i] = items;
};

const listConstructor = listContent.map(function(content){
    let li = document.createElement('li');
    li.innerHTML= `${content}`;
    return li;
});

const ol = document.createElement("ol");

for(let key of listConstructor){
    ol.appendChild(key);
}

document.body.appendChild(ol);

let timer = document.createElement("div");

let j = 9;

setInterval(function(){
    timer.innerText = j;
    if(j<4) {
        timer.style.color="red";
    }
    j--;
},1000);

document.body.insertBefore(timer,ol);

setTimeout(() => {
    ol.parentElement.removeChild(ol);
    timer.parentElement.removeChild(timer);
},10000);


timer.classList.add("timer-style");
ol.classList.add("list-style");



